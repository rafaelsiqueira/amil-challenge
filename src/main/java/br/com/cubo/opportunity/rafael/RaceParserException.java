package br.com.cubo.opportunity.rafael;

public class RaceParserException extends RuntimeException {

    public RaceParserException() {
    }

    public RaceParserException(String message) {
        super(message);
    }

    public RaceParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public RaceParserException(Throwable cause) {
        super(cause);
    }

    public RaceParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}