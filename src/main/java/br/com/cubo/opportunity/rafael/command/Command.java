package br.com.cubo.opportunity.rafael.command;

import br.com.cubo.opportunity.rafael.model.Race;

public interface Command {
    void execute(Race race);
}
