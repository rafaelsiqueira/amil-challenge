package br.com.cubo.opportunity.rafael.model;

import org.joda.time.LocalTime;

public class Lap implements Cloneable {

    private LocalTime timestamp;
    private LocalTime lapTime;
    private int number;
    private double speedAvg;
    private Racer racer;

    public Lap() {
    }

    /**
     * @param timestamp
     * @return
     */
    public Lap withTimestamp(LocalTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    /**
     * @param number
     * @return
     */
    public Lap withNumber(int number) {
        this.number = number;
        return this;
    }

    /**
     * @param racer
     * @return
     */
    public Lap withRacer(Racer racer) {
        this.racer = racer;
        return this;
    }

    /**
     * @param lapTime
     * @return
     */
    public Lap withLapTime(LocalTime lapTime) {
        this.lapTime = lapTime;
        return this;
    }

    /**
     * @param speedAvg
     * @return
     */
    public Lap withSpeedAvg(double speedAvg) {
        this.speedAvg = speedAvg;
        return this;
    }

    /**
     * @return
     */
    public LocalTime getTimestamp() {
        return timestamp;
    }

    /**
     * @return
     */
    public int getNumber() {
        return number;
    }

    /**
     * @return
     */
    public LocalTime getLapTime() {
        return lapTime;
    }

    /**
     * @return
     */
    public Double getSpeedAvg() {
        return speedAvg;
    }

    /**
     * @return
     */
    public Racer getRacer() {
        return racer;
    }

    /**
     *
     * @return
     */
    public Lap clone() {
        try {
            return (Lap) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Lap();
        }
    }
}