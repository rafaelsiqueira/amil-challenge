package br.com.cubo.opportunity.rafael.command;

import br.com.cubo.opportunity.rafael.Constants;
import br.com.cubo.opportunity.rafael.model.Lap;
import br.com.cubo.opportunity.rafael.model.Race;
import br.com.cubo.opportunity.rafael.model.Racer;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class GridCommand implements Command {

    @Override
    public void execute(Race race) {
        int position = 1;

        String lineFormat = "%20s%20s%20s%30s%30s%30s%30s%30s";

        System.out.println(
            String.format(lineFormat, new Object[]{
                "**Posição Chegada**",
                "**Código Piloto**",
                "**Nome Piloto**",
                "**Qtde Voltas Completadas**",
                "**Tempo Total de Prova**",
                "**Melhor Volta**",
                "**Velocidade Média**",
                "**Tempo após vencedor**"
            })
        );

        Racer winner = null;

        for (Racer racer : race.finishGrid()) {
            Lap bestLap = racer.getBestLap();

            if (winner == null) {
                winner = racer;
            }

            System.out.println(String.format(
                lineFormat, new Object[]{
                    formattedPosition(position),
                    racer.getId(),
                    racer.getName(),
                    String.valueOf(racer.getNumOfLaps()),
                    racer.getTotalRaceTime().toString(),
                    formattedBestLap(bestLap),
                    formattedSpeed(racer.getSpeedAvg()),
                    formattedTimeDistance(winner, racer)
                })
            );

            position++;
        }
    }

    private String formattedBestLap(Lap bestLap) {
        return String.valueOf(bestLap.getNumber()) + "ª (" + bestLap.getLapTime().toString() + ")";
    }

    private String formattedPosition(int position) {
        return position + "º";
    }

    private String formattedSpeed(double speed) {
        return String.format("%.2f", speed) + " km/h";
    }

    private String formattedTimeDistance(Racer winner, Racer racer) {
        if (winner == racer)
            return "----";

        if (racer.getNumOfLaps() != Constants.RACE_FINAL_LAP)
            return "N/D";

        PeriodFormatter formatter = new PeriodFormatterBuilder()
            .appendMinutes()
            .appendSeparator(":")
            .appendSecondsWithMillis()
            .appendSuffix("s")
            .toFormatter();

        return winner.intervalBetween(racer).toString(formatter);
    }
}