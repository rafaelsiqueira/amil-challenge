package br.com.cubo.opportunity.rafael.parser;

import br.com.cubo.opportunity.rafael.Constants;
import br.com.cubo.opportunity.rafael.RaceParserException;
import br.com.cubo.opportunity.rafael.model.Lap;
import br.com.cubo.opportunity.rafael.model.Race;
import br.com.cubo.opportunity.rafael.model.Racer;
import org.joda.time.LocalTime;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class RaceParser {
    private static final Map<String, Racer> RACERS = new HashMap<>();

    public static Race parse(String raceLogFileName) throws IOException {
        RACERS.clear();

        try (Stream<String> stream = Files.lines(Paths.get(raceLogFileName))) {
            Race race = processLaps(stream);
            race.addRacer(RACERS.values());
            return race;

        } catch (IOException e) {
            throw e;
        }
    }

    public static Race processLaps(Stream<String> stream) {
        Race race = new Race();
        stream.skip(1).forEach(line -> race.addLap(parseLap(line)));
        return race;
    }

    public static Lap parseLap(String line) {
        try {
            StringTokenizer token = new StringTokenizer(line, "\t");

            if (token.countTokens() != Constants.RACE_LOG_TOKEN_COUNT) {
                throw new IllegalArgumentException("Linha em formato inválido");
            }

            Lap lap = new Lap();

            return lap
                    .withTimestamp(parseTimestamp(token.nextElement()))
                    .withRacer(parseRacer(token.nextElement(), lap))
                    .withNumber(parseNumber(token.nextElement()))
                    .withLapTime(parseLapTimestamp(token.nextElement()))
                    .withSpeedAvg(parseSpeedAvg(token.nextElement()));

        } catch (Exception ex) {
            throw new RaceParserException(ex);
        }
    }

    public static LocalTime parseTimestamp(Object token) {
        return LocalTime.parse(token.toString());
    }

    private static int parseNumber(Object token) {
        return Integer.parseInt(token.toString());
    }

    private static LocalTime parseLapTimestamp(Object token) {
        String[] timeParts = token.toString().split(":");
        String[] secondParts = timeParts[1].split("\\.");

        return new LocalTime(0,
            Integer.parseInt(timeParts[0]),
            Integer.parseInt(secondParts[0]),
            secondParts.length == 2 ? Integer.parseInt(secondParts[1]) : 0
        );
    }

    public static Racer parseRacer(Object token, Lap lap) {
        String[] racerParts = token.toString().split("–");

        String id = racerParts[0].trim();
        String name = racerParts[1].trim();

        Racer racer = addOrGetRacerFromCache(id, name);
        racer.addLap(lap);

        return racer;
    }

    public static Racer addOrGetRacerFromCache(String id, String name) {
        Racer racer;
        if (RACERS.containsKey(id)) {
            racer = RACERS.get(id);
        } else {
            racer = new Racer(id, name);
            RACERS.put(id, racer);
        }
        return racer;
    }

    protected static double parseSpeedAvg(Object token) {
        return Double.valueOf(token.toString().replace(",", "."));
    }


}