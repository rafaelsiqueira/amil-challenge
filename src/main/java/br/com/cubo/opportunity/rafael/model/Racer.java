package br.com.cubo.opportunity.rafael.model;

import org.joda.time.LocalTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Racer {

    private String id;
    private String name;
    private List<Lap> laps;

    public Racer(String id, String name) {
        this.id = id;
        this.name = name;
    }

    /**
     * @param lap
     */
    public void addLap(Lap lap) {
        getLaps().add(lap);
    }

    /**
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @return
     */
    public List<Lap> getLaps() {
        if (laps == null) {
            laps = new ArrayList<>();
        }
        return laps;
    }

    /**
     * Get numer of laps completed by racer
     * @return
     */
    public int getNumOfLaps() {
        return getLaps().size();
    }

    /**
     * Get total race time of racer
     * @return
     */
    public LocalTime getTotalRaceTime() {
        long totalRaceTime = 0;
        for (Lap lap : getLaps()) {
            totalRaceTime += lap.getLapTime().getMillisOfDay();
        }
        return LocalTime.fromMillisOfDay(totalRaceTime);
    }

    /**
     * Get the best lap of racer
     * @return
     */
    public Lap getBestLap() {
        Optional<Lap> lap = getLaps()
                .stream()
                .sorted(Comparator.comparing(Lap::getLapTime))
                .findFirst();

        return lap.isPresent() ? lap.get() : null;
    }

    /**
     * Get average speed of racer
     * @return
     */
    public double getSpeedAvg() {
        return getLaps()
            .stream()
            .mapToDouble(Lap::getSpeedAvg)
            .average()
            .getAsDouble();
    }

    /**
     * Calculates arrival distance (time) between two racers
     *
     * @param racer
     * @return
     */
    public Period intervalBetween(Racer racer) {
        return Period.fieldDifference(getTotalRaceTime(), racer.getTotalRaceTime()).toPeriod();
    }
}