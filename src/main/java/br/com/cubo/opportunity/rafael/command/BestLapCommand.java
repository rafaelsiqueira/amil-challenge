package br.com.cubo.opportunity.rafael.command;

import br.com.cubo.opportunity.rafael.model.Lap;
import br.com.cubo.opportunity.rafael.model.Race;

public class BestLapCommand implements Command {

    @Override
    public void execute(Race race) {
        System.out.println("\n\n********** MELHOR VOLTA DA CORRIDA **********");
        Lap bestLap = race.bestLap();
        System.out.println(bestLap.getNumber() + "ª\t" + bestLap.getLapTime().toString() + "\t" + bestLap.getRacer().getName());
    }

}