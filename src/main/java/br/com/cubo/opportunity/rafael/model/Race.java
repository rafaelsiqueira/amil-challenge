package br.com.cubo.opportunity.rafael.model;

import java.util.*;
import java.util.stream.Collectors;

public class Race {

    private List<Racer> racers;
    private List<Lap> laps;

    public void addLap(Lap lap) {
        getLaps().add(lap);
    }

    public List<Lap> getLaps() {
        if (laps == null) {
            laps = new ArrayList<>();
        }
        return laps;
    }

    public void addRacer(Collection<Racer> racers) {
        getRacers().addAll(racers);
    }

    public List<Racer> getRacers() {
        if (racers == null) {
            racers = new ArrayList<>();
        }
        return racers;
    }

    public List<Racer> finishGrid() {
        return getRacers()
                .stream()
                .sorted(byTotalRaceTime())
                .sorted(byNumberOfLaps().reversed())
                .collect(Collectors.toList());
    }

    public Lap bestLap() {
        Optional<Lap> lap = getLaps()
                .stream()
                .sorted(byRaceLapTime())
                .findFirst();

        return lap.isPresent() ? lap.get() : null;
    }

    private static Comparator<Lap> byRaceLapTime() {
        return Comparator.comparing(Lap::getLapTime);
    }

    private static Comparator<Racer> byTotalRaceTime() {
        return Comparator.comparing(Racer::getTotalRaceTime);
    }

    private static Comparator<Racer> byNumberOfLaps() {
        return Comparator.comparing(Racer::getNumOfLaps);
    }
}