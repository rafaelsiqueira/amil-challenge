package br.com.cubo.opportunity.rafael;

import br.com.cubo.opportunity.rafael.command.BestLapCommand;
import br.com.cubo.opportunity.rafael.command.Command;
import br.com.cubo.opportunity.rafael.command.GridCommand;
import br.com.cubo.opportunity.rafael.model.Race;
import br.com.cubo.opportunity.rafael.parser.RaceParser;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        try {

            checkArguments(args);

            Race race = RaceParser.parse(args[0]);

            Command[] commands = new Command[]{
                new GridCommand(),
                new BestLapCommand()
            };

            Arrays.stream(commands)
                .forEach(command -> command.execute(race));

        } catch (IllegalArgumentException | UncheckedIOException | IOException ex) {
            usage();

        } catch (RaceParserException ex) {
            usageFormat();
        }
    }

    private static void checkArguments(String[] args) {
        if (args == null || args.length == 0) {
            throw new IllegalArgumentException("Log de corrida não informado e/ou inválido.");
        }

        Path p = Paths.get(args[0]);
        if (!Files.exists(p) || p.toFile().isDirectory()) {
            throw new IllegalArgumentException("Log de corrida inválido.");
        }
    }

    private static void usage() {
        System.out.println("Usage: gradle run -PlogFile=\"race-log-file.txt\"");
    }

    private static void usageFormat() {
        System.out.println("Arquivo em formato inválido.");
        System.out.println("O arquivo deve estar no seguinte formato (campos separados por TAB):");
        System.out.println("Hora\tPiloto\tNº Volta\tTempo Volta\tVelocidade média da volta");
        System.out.println("23:49:08.277\tCOD – NOME PILOTO\t1\t1:02.852\t44,275");
    }

}