package br.com.cubo.opportunity.rafael.parser;

import br.com.cubo.opportunity.rafael.CommonTest;
import br.com.cubo.opportunity.rafael.model.Lap;
import br.com.cubo.opportunity.rafael.model.Race;
import br.com.cubo.opportunity.rafael.model.Racer;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class RaceParserTest extends CommonTest {
    String logFile = RaceParserTest.class.getResource("/example1.txt").getPath();

    @Test
    public void parseTest() throws IOException {
        Race race = RaceParser.parse(logFile);

        Assert.assertEquals(23, race.getLaps().size());
        Assert.assertEquals(6, race.getRacers().size());
    }

    @Test
    public void parseLapTest() {
        Lap lap = RaceParser.parseLap("23:49:30.976\t015 – F.ALONSO\t1\t1:18.456\t35,47");

        Assert.assertEquals("23:49:30.976", lap.getTimestamp().toString());
        Assert.assertEquals("F.ALONSO", lap.getRacer().getName());
        Assert.assertEquals(1, lap.getNumber());
        Assert.assertEquals("00:01:18.456", lap.getLapTime().toString());
        Assert.assertEquals(new Double(35.47d), lap.getSpeedAvg());
    }

    @Test
    public void parseRacerTest() {
        Lap lap = RaceParser.parseLap("23:49:30.976\t015 – F.ALONSO\t1\t1:18.456\t35,47");
        Racer racer = RaceParser.parseRacer("015 – F.ALONSO", lap);

        Assert.assertEquals("015", racer.getId());
        Assert.assertEquals("F.ALONSO", racer.getName());
    }

    @Test
    public void addOrGetRacerFromCacheTest() {
        Racer racer01 = RaceParser.addOrGetRacerFromCache("001", "Nome1");
        Racer racer02 = RaceParser.addOrGetRacerFromCache("002", "Nome2");

        Assert.assertNotEquals(racer01, racer02);

        Assert.assertEquals(racer01, RaceParser.addOrGetRacerFromCache("001", "Nome1"));
        Assert.assertEquals(racer02, RaceParser.addOrGetRacerFromCache("002", "Nome2"));

        Assert.assertNotEquals(racer01, RaceParser.addOrGetRacerFromCache("002", "Nome2"));
        Assert.assertNotEquals(racer02, RaceParser.addOrGetRacerFromCache("001", "Nome1"));
    }
}