package br.com.cubo.opportunity.rafael;

import org.junit.Before;

import java.io.*;
import java.util.stream.Collectors;

public class CommonTest {

    private static ByteArrayOutputStream out;

    @Before
    public void beforeTest() {
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
    }

    protected String getOutput() {
        InputStream input = new ByteArrayInputStream(out.toByteArray());
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input))) {
            return buffer.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

}
