package br.com.cubo.opportunity.rafael;

import org.junit.Assert;
import org.junit.Test;

public class AppTest extends CommonTest {

    @Test
    public void runExample1Test() {
        String logFile = AppTest.class.getResource("/example1.txt").getPath();
        App.main(new String[]{ logFile });

        String[] output = getOutput().split("\n");
        Assert.assertTrue(output[0].contains("**Código Piloto**"));
        Assert.assertTrue(output[1].contains("F.MASSA"));
        Assert.assertTrue(output[2].contains("K.RAIKKONEN"));
        Assert.assertTrue(output[3].contains("R.BARRICHELLO"));
        Assert.assertTrue(output[4].contains("M.WEBBER"));
        Assert.assertTrue(output[5].contains("F.ALONSO"));
        Assert.assertTrue(output[6].contains("S.VETTEL"));
    }

    @Test
    public void runExample2Test() {
        String logFile = AppTest.class.getResource("/example2.txt").getPath();
        App.main(new String[]{logFile});

        String[] output = getOutput().split("\n");
        Assert.assertTrue(output[0].contains("**Código Piloto**"));
        Assert.assertTrue(output[1].contains("M.WEBBER"));
        Assert.assertTrue(output[2].contains("F.MASSA"));
        Assert.assertTrue(output[3].contains("K.RAIKKONEN"));
        Assert.assertTrue(output[4].contains("R.BARRICHELLO"));
        Assert.assertTrue(output[5].contains("F.ALONSO"));
        Assert.assertTrue(output[6].contains("S.VETTEL"));
    }

    @Test
    public void runExample3Test() {
        String logFile = AppTest.class.getResource("/example3.txt").getPath();
        App.main(new String[]{logFile});

        String[] output = getOutput().split("\n");
        Assert.assertTrue(output[0].contains("Arquivo em formato inválido."));
    }

    @Test
    public void runWithNullArgment() {
        App.main(null);
        String[] output = getOutput().split("\n");
        Assert.assertTrue(output[0].contains("Usage:"));
    }

    @Test
    public void runWithEmptyArgment() {
        App.main(new String[]{});
        String[] output = getOutput().split("\n");
        Assert.assertTrue(output[0].contains("Usage:"));
    }
}